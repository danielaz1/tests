package db;

import db.repository.*;
import db.uow.HsqlUnitOfWork;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class RepositoryCatalog {

	private Connection connection;
	private HsqlUnitOfWork hsqlUnitOfWork;
	private ActorRepository actorRepository;
	private DirectorRepository directorRepository;
	private EpisodeRepository episodeRepository;
	private SeasonRepository seasonRepository;
	private TvSeriesRepository tvSeriesRepository;
	private TvSeriesActorRepository tvSeriesActorRepository;

	private HashMap<String, String> createTables;

	public RepositoryCatalog(Connection connection, HsqlUnitOfWork hsqlUnitOfWork) {
		this.connection = connection;
		this.hsqlUnitOfWork = hsqlUnitOfWork;
		tvSeriesActorRepository = new TvSeriesActorRepository(connection, hsqlUnitOfWork, this);
		directorRepository = new DirectorRepository(connection, hsqlUnitOfWork, this);
		episodeRepository = new EpisodeRepository(connection, hsqlUnitOfWork, this);
		seasonRepository = new SeasonRepository(connection, hsqlUnitOfWork, this);
		tvSeriesRepository = new TvSeriesRepository(connection, hsqlUnitOfWork, this);
		actorRepository = new ActorRepository(connection, hsqlUnitOfWork, this);
		createTables = new HashMap<>();
		setCreateTables();
		createTables();
	}

	private void createTables() {
		try {
			ResultSet rs = connection.getMetaData().getTables(null, "PUBLIC", null, null);
			for (String tableName : createTables.keySet()) {
				boolean found = false;
				while (rs.next()) {
					if (tableName.equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
						found = true;
						break;
					}
				}
				rs.first();
				if (! found) {
					Statement createTable = connection.createStatement();
					createTable.executeUpdate(createTables.get(tableName));
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private void setCreateTables() {
		createTables.put(actorRepository.getTableName(), actorRepository.getCreateTableSql());
		createTables.put(directorRepository.getTableName(), directorRepository.getCreateTableSql());
		createTables.put(episodeRepository.getTableName(), episodeRepository.getCreateTableSql());
		createTables.put(seasonRepository.getTableName(), seasonRepository.getCreateTableSql());
		createTables.put(tvSeriesRepository.getTableName(), tvSeriesRepository.getCreateTableSql());
	}

	public ActorRepository actors() {
		return actorRepository;
	}

	public DirectorRepository directors() {
		return directorRepository;
	}

	public EpisodeRepository episodes() {
		return episodeRepository;
	}

	public SeasonRepository seasons() {
		return seasonRepository;
	}

	public TvSeriesRepository tvSeries() {
		return tvSeriesRepository;
	}

	public TvSeriesActorRepository getTvSeriesActorRepository() {
		return tvSeriesActorRepository;
	}
}
