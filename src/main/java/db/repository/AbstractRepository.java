package db.repository;

import db.RepositoryCatalog;
import db.uow.HsqlUnitOfWork;
import db.uow.UnitOfWorkRepository;
import domain.Entity;

import java.sql.*;
import java.util.ArrayList;

public abstract class AbstractRepository<TEntity extends Entity> implements Repository<TEntity>, UnitOfWorkRepository {

	protected Connection connection;
	protected HsqlUnitOfWork hsqlUnitOfWork;
	protected RepositoryCatalog repositoryCatalog;

	abstract protected String findStatement();

	abstract protected String getAllStatement();

	abstract protected String insertStatement();

	abstract protected String updateStatement();

	abstract protected String removeStatement();

	protected abstract String removeAllStatement();

	abstract protected TEntity doLoad(ResultSet rs) throws SQLException;

	abstract protected void parametrizeInsertStatement(PreparedStatement statement, TEntity entity) throws SQLException;

	abstract protected void parametrizeUpdateStatement(PreparedStatement statement, TEntity entity) throws SQLException;

	public AbstractRepository(Connection connection, HsqlUnitOfWork hsqlUnitOfWork, RepositoryCatalog repositoryCatalog) {
		this.connection = connection;
		this.hsqlUnitOfWork = hsqlUnitOfWork;
		this.repositoryCatalog = repositoryCatalog;
	}

	public abstract String getTableName();

	public abstract String getCreateTableSql();

	@Override
	public void persistAdd(Entity e) {
		if (e == null) {
			throw new NullPointerException("entity cannot be null");
		}
		addEntity((TEntity) e);
	}

	@Override
	public void persistUpdate(Entity entity) {
		if (entity == null) {
			throw new NullPointerException("entity cannot be null");
		}
		PreparedStatement updateStatement;
		try {
			updateStatement = connection.prepareStatement(updateStatement());

			parametrizeUpdateStatement(updateStatement, (TEntity) entity);

			updateStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void persistDelete(Entity e) {
		if (e == null) {
			throw new NullPointerException("entity cannot be null");
		}
		delete(e.getId());
	}

	@Override
	public void add(TEntity entity) {
		if (entity == null) {
			throw new NullPointerException("entity cannot be null");
		}
		hsqlUnitOfWork.markAsNew(entity, this);
	}

	@Override
	public void modify(TEntity entity) {
		if (entity == null) {
			throw new NullPointerException("entity cannot be null");
		}
		hsqlUnitOfWork.markAsDirty(entity, this);
	}

	@Override
	public void remove(TEntity entity) {
		if (entity == null) {
			throw new NullPointerException("entity cannot be null");
		}
		hsqlUnitOfWork.markAsDeleted(entity, this);
	}

	@Override
	public TEntity withId(long id) {
		PreparedStatement findStatement;
		try {
			findStatement = connection.prepareStatement(findStatement());
			findStatement.setLong(1, id);
			ResultSet rs = findStatement.executeQuery();
			if (rs.next()) {
				return doLoad(rs);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public ArrayList<TEntity> getAll() {
		ArrayList<TEntity> results = new ArrayList<>();
		PreparedStatement getAllStmt;
		try {
			getAllStmt = connection.prepareStatement(getAllStatement());
			ResultSet rs = getAllStmt.executeQuery();
			while (rs.next()) {
				TEntity result = doLoad(rs);
				results.add(result);
			}
			return results;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	protected long addEntity(TEntity entity) {
		long key = 0;
		try {
			PreparedStatement addStatement = connection.prepareStatement(insertStatement(), Statement.RETURN_GENERATED_KEYS);
			parametrizeInsertStatement(addStatement, entity);
			addStatement.executeUpdate();
			ResultSet generatedKeys = addStatement.getGeneratedKeys();
			if (generatedKeys.next()) {
				key = generatedKeys.getLong(1);
			}
			entity.setId(key);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return key;
	}

	protected void delete(long id) {
		PreparedStatement removeStatement;
		try {
			removeStatement = connection.prepareStatement(removeStatement());

			removeStatement.setLong(1, id);

			removeStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void removeAll() {
		try {
			Statement removeAllStmt = connection.createStatement();
			removeAllStmt.executeQuery(removeAllStatement());

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
