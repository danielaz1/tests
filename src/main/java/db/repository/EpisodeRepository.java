package db.repository;

import db.RepositoryCatalog;
import db.uow.HsqlUnitOfWork;
import domain.Episode;

import java.sql.*;
import java.time.Duration;
import java.util.ArrayList;

public class EpisodeRepository extends AbstractRepository<Episode> {

	private static final String TABLE_NAME = "episode";
	private static final String CREATE_TABLE = "CREATE TABLE episode (id bigint GENERATED BY DEFAULT AS IDENTITY, " +
			"name varchar(20), number integer, duration integer, release_date date, season_id bigint)";
	private static final String SELECT_BY_ID_SQL = "SELECT * FROM episode where id=?";
	private static final String SELECT_SQL = "SELECT * FROM episode";
	private static final String INSERT_SQL = "INSERT INTO episode (name, number, duration, release_date, season_id) VALUES (?,?,?,?,?)";
	private static final String UPDATE_SQL = "UPDATE episode SET name=?, number=?, duration=?, release_date=? WHERE ID=?";
	private static final String DELETE_SQL = "Delete from episode where id=?";
	private static final String SELECT_BY_SEASON_ID_SQL = "SELECT * FROM episode WHERE SEASON_ID=?";
	private static final String REMOVE_ALL = "DELETE from episode";

	public EpisodeRepository(Connection connection, HsqlUnitOfWork hsqlUnitOfWork, RepositoryCatalog repositoryCatalog) {
		super(connection, hsqlUnitOfWork, repositoryCatalog);
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public String getCreateTableSql() {
		return CREATE_TABLE;
	}

	@Override
	protected String findStatement() {
		return SELECT_BY_ID_SQL;
	}

	@Override
	protected String getAllStatement() {
		return SELECT_SQL;
	}

	@Override
	protected String insertStatement() {
		return INSERT_SQL;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_SQL;
	}

	@Override
	protected String removeStatement() {
		return DELETE_SQL;
	}

	@Override
	protected String removeAllStatement() {
		return REMOVE_ALL;
	}

	@Override
	protected Episode doLoad(ResultSet rs) throws SQLException {
		Episode episode = new Episode();
		episode.setName(rs.getString("name"));
		episode.setEpisodeNumber(rs.getInt("number"));
		episode.setDuration(Duration.ofSeconds(rs.getInt("duration")));
		episode.setReleaseDate(rs.getDate("release_date").toLocalDate());
		episode.setId(rs.getLong("id"));
		return episode;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Episode episode) throws SQLException {
		statement.setString(1, episode.getName());
		statement.setInt(2, episode.getEpisodeNumber());
		statement.setLong(3, episode.getDuration().getSeconds());
		statement.setDate(4, java.sql.Date.valueOf(episode.getReleaseDate()));
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Episode episode) throws SQLException {
		parametrizeInsertStatement(statement, episode);
		statement.setLong(5, episode.getId());
	}

	public ArrayList<Episode> getBySeasonId(long seasonId) throws SQLException {
		ArrayList<Episode> episodes = new ArrayList<>();
		PreparedStatement selectBySeasonIdStmt = connection.prepareStatement(SELECT_BY_SEASON_ID_SQL);
		selectBySeasonIdStmt.setLong(1, seasonId);
		ResultSet rs = selectBySeasonIdStmt.executeQuery();
		while (rs.next()) {
			Episode episode = doLoad(rs);
			episodes.add(episode);
		}
		return episodes;
	}

	public long addEntity(Episode episode, Long seasonId) {
		long key = 0;
		try {
			PreparedStatement addStatement = connection.prepareStatement(insertStatement(), Statement.RETURN_GENERATED_KEYS);
			parametrizeInsertStatement(addStatement, episode);
			if (seasonId != null) {
				addStatement.setLong(5, seasonId);
			} else {
				addStatement.setNull(5, Types.BIGINT);
			}
			addStatement.executeUpdate();
			ResultSet generatedKeys = addStatement.getGeneratedKeys();
			if (generatedKeys.next()) {
				key = generatedKeys.getLong(1);
			}
			episode.setId(key);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return key;
	}

	@Override
	public long addEntity(Episode episode) {
		return addEntity(episode, null);
	}
}

