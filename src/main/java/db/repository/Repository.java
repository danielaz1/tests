package db.repository;

import java.util.ArrayList;

public interface Repository<TEntity> {

	TEntity withId(long id);

	ArrayList<TEntity> getAll();

	void add(TEntity entity);

	void modify(TEntity entity);

	void remove(TEntity entity);

	void removeAll();
}
