package db.uow;

import domain.Entity;
import domain.EntityState;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class HsqlUnitOfWork implements UnitOfWork {

	private Connection connection;

	private Map<Entity, UnitOfWorkRepository> entities = new LinkedHashMap<>();

	public HsqlUnitOfWork(Connection connection) {
		this.connection = connection;

		try {
			connection.setAutoCommit(false);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void commit() {
		for (Entity entity : entities.keySet()) {
			switch (entity.getState()) {
				case Changed:
					entities.get(entity).persistUpdate(entity);
					break;
				case Deleted:
					entities.get(entity).persistDelete(entity);
					break;
				case New:
					entities.get(entity).persistAdd(entity);
					break;
				case Unchanged:
					break;
				default:
					break;
			}
		}

		try {
			connection.commit();
			entities.clear();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void rollback() {
		entities.clear();
	}

	@Override
	public void markAsNew(Entity entity, UnitOfWorkRepository repository) {
		if (entity == null || repository == null) {
			throw new NullPointerException("argument cannot be null");
		}
		if (entities.containsKey(entity)) {
			throw new IllegalArgumentException("entity has already been added");
		}
		entity.setState(EntityState.New);
		entities.put(entity, repository);

	}

	@Override
	public void markAsDirty(Entity entity, UnitOfWorkRepository repository) {
		if (entity == null || repository == null) {
			throw new NullPointerException("argument cannot be null");
		}
		if (entities.containsKey(entity)) {
			throw new IllegalArgumentException("entity has already been added");
		}
		entity.setState(EntityState.Changed);
		entities.put(entity, repository);
	}

	@Override
	public void markAsDeleted(Entity entity, UnitOfWorkRepository repository) {
		if (entity == null || repository == null) {
			throw new NullPointerException("argument cannot be null");
		}
		if (entities.containsKey(entity)) {
			throw new IllegalArgumentException("entity has already been added");
		}
		entity.setState(EntityState.Deleted);
		entities.put(entity, repository);
	}
}
