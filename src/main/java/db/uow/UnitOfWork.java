package db.uow;

import domain.Entity;

public interface UnitOfWork {

	void commit();

	void rollback();

	void markAsNew(Entity e, UnitOfWorkRepository repository);

	void markAsDirty(Entity e, UnitOfWorkRepository repository);

	void markAsDeleted(Entity e, UnitOfWorkRepository repository);

}
