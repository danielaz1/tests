package domain;

import java.util.ArrayList;
import java.util.List;

public class TvSeries extends Entity {

	private String name;
	private ArrayList<Season> seasons;
	private List<Actor> actors;
	private Director director;

	public TvSeries(String name) {
		this.name = name;
	}

	public TvSeries() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;

	}

	public ArrayList<Season> getSeasons() {
		return seasons;
	}

	public void setSeasons(ArrayList<Season> seasons) {
		this.seasons = seasons;
	}

	public List<Actor> getActors() {
		return actors;
	}

	public void setActors(List<Actor> actors) {
		this.actors = actors;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}
}