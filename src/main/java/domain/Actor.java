package domain;

import java.time.LocalDate;

public class Actor extends Person {

	public Actor(String name, LocalDate dateOfBirth, String biography) {
		super(name, dateOfBirth, biography);
	}

	public Actor() {

	}
}
