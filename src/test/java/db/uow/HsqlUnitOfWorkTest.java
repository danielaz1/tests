package db.uow;

import db.repository.ActorRepository;
import db.repository.TvSeriesRepository;
import domain.Actor;
import domain.Entity;
import domain.EntityState;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.LinkedHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class HsqlUnitOfWorkTest {

	@InjectMocks
	private HsqlUnitOfWork hsqlUnitOfWork;

	@Mock
	private Connection connection;

	@Spy
	private LinkedHashMap<Entity, UnitOfWorkRepository> entitiesMap;

	@Mock
	private ActorRepository actorRepository;

	@Mock
	private TvSeriesRepository tvSeriesRepository;

	private Actor actor;

	@Before
	public void setUp() {
		initMocks(this);
		actor = new Actor("name", LocalDate.of(1990, 2, 2), "biography");
	}

	@Test
	public void markAsNewActor() {
		//when
		hsqlUnitOfWork.markAsNew(actor, actorRepository);

		//then
		assertEquals(EntityState.New, actor.getState());
		assertThat(entitiesMap, IsMapContaining.hasEntry(actor, actorRepository));
	}

	@Test(expected = IllegalArgumentException.class)
	public void markAsNewExistingActor() {
		//given
		entitiesMap.put(actor, actorRepository);

		//when
		hsqlUnitOfWork.markAsNew(actor, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void markAsNewNull() {
		//when
		hsqlUnitOfWork.markAsNew(null, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void markAsNewWithNullRepository() {
		//when
		hsqlUnitOfWork.markAsNew(actor, null);
	}

	@Test
	public void markAsDirtyActor() {
		//when
		hsqlUnitOfWork.markAsDirty(actor, actorRepository);

		//then
		assertEquals(EntityState.Changed, actor.getState());
		assertThat(entitiesMap, IsMapContaining.hasEntry(actor, actorRepository));
	}

	@Test(expected = IllegalArgumentException.class)
	public void markAsDirtyExistingActor() {
		//given
		entitiesMap.put(actor, actorRepository);

		//when
		hsqlUnitOfWork.markAsDirty(actor, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void markAsDirtyNull() {
		//when
		hsqlUnitOfWork.markAsDirty(null, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void markAsDirtyWithNullRepository() {
		//when
		hsqlUnitOfWork.markAsDirty(actor, null);
	}

	@Test
	public void markAsDeletedActor() {
		//when
		hsqlUnitOfWork.markAsDeleted(actor, actorRepository);

		//then
		assertEquals(EntityState.Deleted, actor.getState());
		assertThat(entitiesMap, IsMapContaining.hasEntry(actor, actorRepository));
	}

	@Test(expected = IllegalArgumentException.class)
	public void markAsDeletedExistingActor() {
		//given
		entitiesMap.put(actor, actorRepository);

		//when
		hsqlUnitOfWork.markAsDeleted(actor, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void markAsDeletedNull() {
		//when
		hsqlUnitOfWork.markAsDeleted(null, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void markAsDeletedWithNullRepository() {
		//when
		hsqlUnitOfWork.markAsDeleted(actor, null);
	}

	@Test
	public void commit() throws SQLException {
		//given
		actor.setState(EntityState.New);
		entitiesMap.put(actor, actorRepository);

		//when
		hsqlUnitOfWork.commit();

		//then
		verify(actorRepository).persistAdd(actor);
		verify(connection).commit();
		assertEquals(0, entitiesMap.entrySet().size());
	}

}
