package db.repository;

import db.RepositoryCatalog;
import db.uow.HsqlUnitOfWork;
import domain.Actor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.*;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class ActorRepositoryTest {

	@InjectMocks
	private ActorRepository actorRepository;

	@Mock
	private Connection connection;

	@Mock
	private HsqlUnitOfWork hsqlUnitOfWork;

	@Mock
	private RepositoryCatalog repositoryCatalog;

	@Mock
	private TvSeriesActorRepository tvSeriesActorRepository;

	@Mock
	private ResultSet rs;

	@Mock
	private PreparedStatement stmt;

	private Actor actor;

	@Before
	public void setUp() {
		initMocks(this);
		actor = new Actor("name", LocalDate.of(1990, 2, 2), "biography");
	}

	@Test
	public void addActor() {
		//when
		actorRepository.add(actor);

		//then
		verify(hsqlUnitOfWork).markAsNew(actor, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void addNullThrowsNPException() {
		//when
		actorRepository.add(null);
	}

	@Test
	public void modifyActor() {
		//when
		actorRepository.modify(actor);

		//then
		verify(hsqlUnitOfWork).markAsDirty(actor, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void modifyNullThrowsNPException() {
		//when
		actorRepository.modify(null);
	}

	@Test
	public void removeActor() {
		//when
		actorRepository.remove(actor);

		//then
		verify(hsqlUnitOfWork).markAsDeleted(actor, actorRepository);
	}

	@Test(expected = NullPointerException.class)
	public void removeNullThrowsNPException() {
		//when
		actorRepository.remove(null);
	}

	@Test
	public void persistAddActor() throws SQLException {
		//given
		when(connection.prepareStatement(any(String.class), eq(Statement.RETURN_GENERATED_KEYS))).thenReturn(stmt);
		when(stmt.getGeneratedKeys()).thenReturn(rs);
		when(rs.next()).thenReturn(true);
		when(rs.getLong(1)).thenReturn(1L);

		//when
		actorRepository.persistAdd(actor);

		//then
		long id = actor.getId();
		assertEquals(id, 1L);
	}

	@Test(expected = NullPointerException.class)
	public void persistAddNullThrowsNPException() {
		//when
		actorRepository.persistAdd(null);
	}

	@Test
	public void persistUpdateActor() throws SQLException {
		//given
		actor.setId(1L);
		when(connection.prepareStatement(any(String.class))).thenReturn(stmt);

		//when
		actorRepository.persistUpdate(actor);

		//then
		verify(stmt).setString(1, actor.getName());
		verify(stmt).setDate(2, Date.valueOf(actor.getDateOfBirth()));
		verify(stmt).setString(3, actor.getBiography());
		verify(stmt).setLong(4, actor.getId());
		verify(stmt).executeUpdate();
	}

	@Test(expected = NullPointerException.class)
	public void persistUpdateNullThrowsNPException() {
		//when
		actorRepository.persistUpdate(null);
	}

	@Test
	public void persistDeleteActor() throws SQLException {
		//given
		actor.setId(1L);
		when(connection.prepareStatement(any(String.class))).thenReturn(stmt);
		when(repositoryCatalog.getTvSeriesActorRepository()).thenReturn(tvSeriesActorRepository);

		//when
		actorRepository.persistDelete(actor);

		//then
		verify(tvSeriesActorRepository).removeByActorId(actor.getId());
		verify(stmt).setLong(1, actor.getId());
		verify(stmt).executeUpdate();
	}

	@Test(expected = NullPointerException.class)
	public void persistDeleteNullThrowsNPException() {
		//when
		actorRepository.persistDelete(null);
	}

	@Test
	public void findActorWithId() throws SQLException {
		//given
		Long id = 1L;

		when(connection.prepareStatement(any(String.class))).thenReturn(stmt);
		when(stmt.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getLong("id")).thenReturn(1L);
		when(rs.getString("name")).thenReturn(actor.getName());
		when(rs.getDate("date_of_birth")).thenReturn(Date.valueOf(actor.getDateOfBirth()));
		when(rs.getString("biography")).thenReturn(actor.getBiography());

		//when
		Actor result = actorRepository.withId(id);

		//then
		assertEquals(id, result.getId());
		assertEquals(actor.getName(), result.getName());
		assertEquals(actor.getBiography(), result.getBiography());
		assertEquals(actor.getDateOfBirth(), result.getDateOfBirth());
	}

}
