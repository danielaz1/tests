package db;

import db.uow.HsqlUnitOfWork;
import domain.Actor;
import domain.TvSeries;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public class UnitOfWorkTest {

	private final static String NAME_1 = "Name1";
	private final static String NAME_2 = "Name2";
	private final static String BIOGRAPHY_1 = "Biography1";
	private final static LocalDate DATE_1 = LocalDate.of(1990, 1, 1);

	private Connection connection;
	private HsqlUnitOfWork hsqlUnitOfWork;
	private RepositoryCatalog repositoryCatalog;

	private Actor actor;
	private Actor actor2;

	@Before
	public void setUp() {
		connection = new HsqldbConnection().getConnection();
		hsqlUnitOfWork = new HsqlUnitOfWork(connection);
		repositoryCatalog = new RepositoryCatalog(connection, hsqlUnitOfWork);

		actor = new Actor(NAME_1, DATE_1, BIOGRAPHY_1);
		actor2 = new Actor(NAME_2, DATE_1, BIOGRAPHY_1);
	}

	@Test
	public void commitNoOperations() {
		hsqlUnitOfWork.commit();
	}

	@Test(expected = NullPointerException.class)
	public void commitWrongAndRightOperation() {
		repositoryCatalog.actors().modify(actor);
		repositoryCatalog.actors().add(actor2);

		hsqlUnitOfWork.commit();
	}

	@Test
	public void commit() {
		repositoryCatalog.actors().add(actor);
		repositoryCatalog.actors().add(actor2);

		hsqlUnitOfWork.commit();

		List<Actor> actors = repositoryCatalog.actors().getAll();

		assertNotNull(actors);
		assertNotEquals(0, actors.size());
		assertNotNull(actor.getId());
		assertNotNull(actor2.getId());
	}

	@Test
	public void rollbackAndCommit() {
		repositoryCatalog.actors().add(actor);

		hsqlUnitOfWork.rollback();

		repositoryCatalog.actors().add(actor2);

		hsqlUnitOfWork.commit();

		assertNull(actor.getId());

		Long id2 = actor2.getId();
		assertNotNull(id2);
		assertNotNull(repositoryCatalog.actors().withId(id2));
	}

	@Test
	public void allOperations() {
		TvSeries tvSeries = new TvSeries(NAME_1);
		hsqlUnitOfWork.markAsNew(tvSeries, repositoryCatalog.tvSeries());
		hsqlUnitOfWork.commit();

		hsqlUnitOfWork.markAsDeleted(tvSeries, repositoryCatalog.tvSeries());

		hsqlUnitOfWork.rollback();

		hsqlUnitOfWork.markAsDirty(tvSeries, repositoryCatalog.tvSeries());

		hsqlUnitOfWork.commit();

		assertNotNull(repositoryCatalog.tvSeries().withId(tvSeries.getId()));
	}

}
